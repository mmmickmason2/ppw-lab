from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Ini adalah biodata saya, Mahasiswa Fasilkom UI 2016'

def index(request):
	if 'user_login' in request.session:
	    response = {'name': mhs_name, 'content': landing_page_content}
	    return render(request, 'index_lab2.html', response)
	else:
		html = 'lab_9/session/login.html'
		return render(request, html, {})