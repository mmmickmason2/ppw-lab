## Checklist

1.  Membuat Halaman Chat Box
    1. [ x ] Tambahkan _lab_6.html_ pada folder templates(Alhamdulillah bisa. Membuat templates aplikasi lab 6 ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/templates/lab_6)
    2. [ x ] Tambahkan _lab_6.css_ pada folder _./static/css_(Alhamdulillah bisa. Mengatur tampilan web ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/css)
    3. [ x ] Tambahkan file _lab_6.js_ pada folder _lab_6/static/js_(Alhamdulillah bisa. Untuk Melakukan Ansychronous Web ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js)
    4. [ x ] Lengkapi potongan kode pada _lab_6.js_ agar dapat berjalan(Alhamdulillah bisa. Sudah dilengkapi ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)

2. Mengimplementasikan Calculator
    1. [ x ] Tambahkan potongan kode ke dalam file _lab_6.html_ pada folder templates(Alhamdulillah bisa. Sudah ditambahkan untuk mengatur peletakan kalkulator ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/templates/lab_6/lab_6.html)
    2. [ x ] Tambahkan potongan kode ke dalam file _lab_6.css_ pada folder _./static/css_(Alhamdulillah bisa. Sudah ditambahkan pada CSS ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/css/lab_6.css)
    3. [ x ] Tambahkan potongan kode ke dalam file _lab_6.js_ pada folder _lab_6/static/js_(Alhamdulillah bisa. Sudah ditambahkan ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)
    4. [ x ] Implementasi fungsi `AC`.(Alhamdulillah bisa. Sudah ditambahkan ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)

3.  Mengimplementasikan select2
    1. [ x ] Load theme default sesuai selectedTheme(Alhamdulillah bisa. Sudah sesuai mengguanakan selectedTheme ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)
    2. [ x ] Populate data themes dari local storage ke select2(Alhamdulillah bisa. Sudah dibuat ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)
    3. [ x ] Local storage berisi themes dan selectedTheme(Alhamdulillah bisa. Sudah diinisiasi ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)
    4. [ x ] Warna berubah ketika theme dipilih(Alhamdulillah bisa. Sudah diubah sesuai warna terpilih --->https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)

4.  Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [ x ]  Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab` di [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md).
    2. [ x ] Pastikan _Code Coverage_ kalian 100%.(Sudah 100% ---> https://gitlab.com/Ismail.AG/ppw-lab/-/jobs/40609418)

###  Challenge Checklist
1. Latihan Qunit
    1. [ x] Implementasi dari latihan Qunit (Alhamdulillah bisa. Sudah ditambahkan ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)
1. Cukup kerjakan salah satu nya saja:
    1. Implementasikan tombol enter pada chat box yang sudah tersedia
        1. [ ] Buatlah sebuah _Unit Test_ menggunakan Qunit
        2. [ ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_ 
    1. Implementasikan fungsi `sin`, `log`, dan `tan`. (HTML sudah tersedia di dalam potongan kode)
        1. [ x ] Buatlah sebuah _Unit Test_ menggunakan Qunit (Alhamdulillah bisa. Sudah ditambahkan ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/lab_6.js)
        2. [ x ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_ (Alhamdulillah bisa. Sudah ditambahkan ---> https://gitlab.com/Ismail.AG/ppw-lab/tree/master/lab_6/static/js/test.js)