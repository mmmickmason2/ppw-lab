from django.test import Client
from django.test import TestCase
from django.urls import resolve

from lab_6.views import index
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab6UnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
	
	def test_lab_6_url_is_exist(self):
		response = self.client.get('/lab-6/')
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		response = self.client.get('/lab-6/')
		self.assertEqual(response.status_code, 200)

	def test_lab6_using_index_func(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)
