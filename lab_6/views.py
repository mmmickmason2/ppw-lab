from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

response = {}
def index(request):
    if 'user_login' in request.session:
    	response['author'] = "Ismail Al Ghani"
    	html = 'lab_6/lab_6.html'
    	return render(request, html, response)
    else:
    	html = 'lab_9/session/login.html'
    	return render(request, html, response)