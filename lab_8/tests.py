from django.test import TestCase
from django.test import Client
from django.urls import resolve
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Lab8UnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_lab_8_url_is_exist(self):
		response = self.client.get('/lab-8/')
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		response = self.client.get('/lab-8/')
		self.assertEqual(response.status_code, 200)